#!/usr/bin/env python2

''' This file is part of um2vnf.

um2vnf is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

um2vnf is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with um2vnf.  If not, see <http://www.gnu.org/licenses/>. '''

import reader, sys

from operator  import itemgetter
from container import umcontainer, umfield, vnffile

umcontainers = {} # key = stashcode
vnffiles = []     # one vnffile per cfg section

reader.read_arguments (sys.argv)
reader.read_config    (sys.argv[1], umcontainers, vnffiles)
reader.read_files     (sys.argv[2], umcontainers)

for vnffile in vnffiles:
    out = open(vnffile.fname, 'w')

    gs = vnffile.get_itemgetter()
    rf = vnffile.get_reference_umfield(gs(umcontainers))
    if rf is None:
        continue

    vnffile.write_vnf_header(out, rf)
    
    for umcontainer in gs(umcontainers):
        umcontainer.write_vnf_component_header(out)

    vnffile.write_vnf_coords_component_header(out)
        
    for umcontainer in gs(umcontainers):
        umcontainer.write_vnf_timesteps(out, vnffile.downsizefunc)

    vnffile.write_vnf_coords_timesteps(out)
        
    out.close()
