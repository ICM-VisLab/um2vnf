# Unified Model (UM) importer

The purpose of this set of tools is to generate so called **VNF** file(s), that
can be read by [VisNow](http://visnow.icm.edu.pl) visualization application.
The file is a header describing UM output files (in a certain format, as they
reside in [ICM](http://www.icm.edu.pl) [UW](http://www.uw.edu.pl)
meteorological archive) for the VisNow. Only requirement is Python 2.7 and
a Linux/Un*x system.

## Terms
* **forecast directory** - the directory containing binary files (IEEE single
precision floating point numbers, big-endian) containing model fields (one file
per horizontal slice (level) per timestep), and maybe some additional
(e.g. meta-data) files
* **stashcode** - a five digit number describing the scalar field as a whole
(with all spatial dimensions and all timesteps), e.g. _00024_ represents surface
temperatures

## Most common use case
Most common usage of the tools provided is just an execution of a collective
script `um2vnf` with a default config file `cfg/example.cfg` located in this
repository:

    /path/um2vnf /path/cfg/default.cfg <forecast_dir>

It should run about 5 seconds locally (up to half a minute via *sshfs* over
reasonable network). Output is a series of *VNF* files described in config
file - these will be put in your current working directory (where you execute
the script).

### Writing custom config file
Problably the best place to start is default config provided with a package (see
above). All the comments within provide a detailed, yet short, introduction.

### Additional notes
Project's avatar is by
[Calusarul](https://commons.wikimedia.org/wiki/User:Calusarul) (own work)
[CC BY-SA 3.0](http://creativecommons.org/licenses/by-sa/3.0).
