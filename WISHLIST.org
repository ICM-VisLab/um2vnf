* Stashcode catalogue
  One should be careful about licensing (parsed MetOffice's Unified Model config
  files would be shipped with the project)
* Project cleanup [1/3]
  - [X] Migrate to Python 2.x
  - [ ] Migrate to Python 3.x
  - [ ] Project as independent Python package
* Further functionality (via config file mostly)
** Pressure levels handling [1/4]
   - [X] Hardcoded for now (levels and their heights according to ISA)
   - [ ] Could be calculated on the go via ISA (scan of levels in the directory)
   - [ ] Custom pressure level handling; ex. assume 2D for graph 3D module
   - [ ] Coordinates handling
** Downsizing [1/3]
   - [X] Hardcoded several static functions (predefined downsizing)
   - [ ] Spatial downsizing (predefined)
   - [ ] Custom user-defined downsizing (via some lexical analyzer generator)
   - [ ] Coordinates downsizing (automatic, user-defined; see above)
** User stashcode data [0/1]
   - [ ] User can define preferred min/max, for example
* Custom grids
  Only C6/c6 grids are supported for now. Automatic downsizing, alignement and
  coordinate handling should be supported
* Verbose output
  - [ ] List of files parsed per component
  - [ ] List of components, etc.
