''' This file is part of um2vnf.

um2vnf is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

um2vnf is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with um2vnf.  If not, see <http://www.gnu.org/licenses/>. '''

import os, sys, re, string

from operator import itemgetter

class umcontainer:
    'Represents the VisNow component as the collection of UM fields (slices)'

    # comp = vector component; use None for scalars
    def __init__(self, scode, name, veclen, veccomp):
        self.scode    = scode
        self.name     = name
        self.veclen   = veclen
        self.veccomp  = veccomp
        self.umfields = [];
        
    def __str__(self):
        ret = "scode=" + str(self.scode) + ";name=" + str(self.name) + ";veccomp=" + str(self.veccomp) + ";files=" + str(len(self.umfields))
        return ret
            
    def write_vnf_component_header(self, out):
        if len(self.umfields) == 0:
            print >> sys.stderr, "WARNING: No files matching stashcode \"" + self.scode + "\" (cfg name \"" + self.name + "\") found"
            return

        if self.veccomp is None:
            out.write(string.join(('component ', self.name, ' float\n'), ''))
        elif self.veclen > 1:
            if self.veccomp == 0: # Print only once
                out.write(string.join(('component ', self.name, ' float'), ''))
                out.write(string.join((', vector ', str(self.veclen), '\n'), ''))

    def write_vnf_timesteps(self, out, downsizefunc):
        if len(self.umfields) == 0:
            return

        for umfield in self.umfields:
            umfield.write_vnf_timestep_slice(out, self.name, self.veccomp, downsizefunc)

class umfield:
    'Represents UM field (i.e. single file after deco decompression)'

    levels_hpa = [1000, 975, 950, 925, 900, 875, 850, 825, 800, 775, 750, 725, 700, 675, 650, 625, 600, 500, 400, 300, 250, 200, 100, 50, 30]
    levels_idx = range(len(levels_hpa))

    pressure_levels_idx = dict(zip(levels_hpa, levels_idx))
                       
    def __init__(self, fname, path):
        self.fname = fname
        self.path  = path
        self.parse_fname(fname)

    def __str__(self):
        return self.path

    def parse_fname(self, fname):
        self.scode = fname[:5]
        
        if fname[31].isalpha():
            # Ex. C6 grid (1.5 km)
            self.hh = int(fname[17:20])
            self.mm = int(fname[20:22])
            self.ss = int(fname[22:24])
            self.gridtype  = fname[31]
            self.gridnum   = fname[32]
            self.leveltype = fname[33:35]
            self.level     = fname[35:40]
        elif fname[31].isdigit():
            # Ex. c4 grid (4.5 km) 
            self.hh = int(fname[17:19])
            self.mm = int(fname[19:21])
            self.ss = int(fname[21:23])
            self.gridtype  = fname[30]
            self.gridnum   = fname[31]
            self.leveltype = fname[32:34]
            self.level     = fname[34:39]           
        else:
            sys.exit("ERROR: Unrecognized filename \"" + self.path + "\"")

        self.ts    = float(self.hh) + float(self.mm)/60.0 + float(self.ss)/3600.0
        self.dim   = 2 if self.level == '00000' else 3

        if self.leveltype == 'pr':
            self.level = str(umfield.pressure_levels_idx[int(self.level)])
        else:
            self.level = str(int(self.level) - 1)
        
    def get_start(self):
        'Returns forecast start date'
        return self.fname[6:16]

    def grid_size(self, align = False):
        "Returns the dimension tuple of a given grid"
        grid = self.gridtype + self.gridnum

        if align:
            grid = grid.lower()
        
        if grid == 'C6':
            return (1024, 1472)
        elif grid == 'c6':
            return (1024, 1471)
        elif grid == 'C4' or grid == 'C5':
            return (448, 616)
        elif grid == 'c4' or grid == 'c5':
            return (448, 615)
        sys.exit('ERROR: grid ' + grid + ' not implemented')

    def grid_is_uv(self):
        'Check, if the planar grid is of type u, v or w (second dim is less by one)'        
        return self.gridtype.isupper()
    
    def write_vnf_timestep_slice(self, out, cname, veccomp, downsizefunc):
        if not downsizefunc(self):
            return
        
        out.write(string.join(('file ', os.path.join(self.path, self.fname), ' binary\n'), ''))
        out.write(string.join(('timestep ', str(self.ts), '\n'), ''))
        out.write('tile ')

        planar_dims = self.grid_size()        
        
        jmin = 0
        jmax = planar_dims[0] - 1

        out.write(string.join((str(jmin), str(jmax)), ':'))
        out.write(' ');

        imin = 0
        imax = planar_dims[1] - 1
        if self.grid_is_uv():
            imin -= 1
            imax -= 1

        out.write(string.join((str(imin), str(imax)), ':'))

        if self.dim == 3:
            out.write(' ');
            out.write(string.join((self.level, self.level), ':'))

        out.write(string.join((', ', cname), ''))
        if veccomp is not None:
            out.write(string.join(('.', str(veccomp)), ''))
        out.write('\n')
        out.write('end\n')

    @staticmethod
    def DOWNSIZE_NONE(umfield):
        return True

    @staticmethod
    def DOWNSIZE_1H_TS(umfield):
        mm = umfield.mm
        return mm == 0

    @staticmethod
    def DOWNSIZE_18H(umfield):
        hh = umfield.hh
        mm = umfield.mm        
        ss = umfield.ss
        return (hh < 18) or (hh == 18 and mm == 0 and ss == 0)
        
class vnffile:
    'Represents the VNF output file (i.e. section in a cfg file)'
    
    def __init__(self, vnfieldname, fname, scodes, coords, downsizetype = 'NONE'):
        self.vnfieldname = vnfieldname
        self.fname  = fname
        self.scodes = scodes
        self.coords = coords

        # Check if given downsize function is implemented
        umfield_class    = globals()['umfield']
        downsizefuncname = 'DOWNSIZE_' + downsizetype.upper()
        if not hasattr(umfield_class, downsizefuncname):
            sys.exit('ERROR: Not recognized downsize type \"' + downsizetype + '\". Check your config file')
        
        # Get the function (from the instance) that we need to call
        self.downsizefunc = getattr(umfield_class, downsizefuncname)

    def __str__(self):
        return self.vnfieldname + ';' + self.fname + ';' + str(self.scodes) + ';' + str(self.coords) + ';' + str(self.downsizefunc)

    def get_itemgetter(self):
        return itemgetter(*self.scodes)

    def write_vnf_header(self, out, reference_field):
        out.write('#VisNow regular field\n')

        planar_dims = reference_field.grid_size(True)
        
        out.write(string.join(('field ', self.vnfieldname, '_', reference_field.get_start()), ''))
        out.write(string.join((', dims', str(planar_dims[0]), str(planar_dims[1])), ' '))

        if reference_field.leveltype == 'pr':
            out.write(' 25')
        elif reference_field.dim == 3:
            out.write(' 70')
        
        if self.coords != "":
            out.write(', coords')
            
        out.write('\n')

    def get_reference_umfield(self, umcontainers):
        reference_stashcode = None
        reference_field = None
        
        for umcontainer in umcontainers:
            if len(umcontainer.umfields) > 0:
                if reference_field is None:
                    reference_stashcode = umcontainer.scode
                    reference_field = umcontainer.umfields[0]
                elif umcontainer.umfields[0].dim != reference_field.dim:
                    sys.exit("ERROR: Fields with different dimensions (stashcodes: " + reference_stashcode + " and " + umcontainer.scode + ") found in one section \"" + self.vnfieldname + "\" of config file")
                elif umcontainer.umfields[0].leveltype != reference_field.leveltype and reference_field.dim == 3: # All 2-dimensional fields are OK
                    sys.exit("ERROR: Fields with different level types (mixed pressure/model levels): stashcodes: " + reference_stashcode + " and " + umcontainer.scode + ") found in one section \"" + self.vnfieldname + "\" of config file")

        if reference_field is None:
            print >> sys.stderr, "WARNING: No files found to be included in VNF \"" + vnffile.fname + "\""
            
        return reference_field

    def write_vnf_coords_component_header(self, out):
        coords = open(self.coords, 'r')
        lines = coords.readlines()

        for line in lines:
            if line.startswith("component "):
                out.write(line)

    def write_vnf_coords_timesteps(self, out):
        coords = open(self.coords, 'r')
        lines = coords.readlines()

        coords_data = self.coords[:-1] + 'd'
        out.write("file " + coords_data + " binary\n")
        out.write("timestep 0.0\n")
        
        list_components = False
        for line in lines:
            if list_components:
                out.write(line)
            elif line.startswith("file "):
                list_components = True

        out.write('end\n')
