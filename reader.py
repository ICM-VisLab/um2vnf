''' This file is part of um2vnf.

um2vnf is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

um2vnf is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with um2vnf.  If not, see <http://www.gnu.org/licenses/>. '''

import os, sys, re

import ConfigParser

from os import listdir
from os.path import isfile, join

from container import umcontainer, umfield, vnffile

def usage():
    print "Usage: ./um2vnf <cfg_file> <forecast_dir>"

def read_arguments(argv):
    if len(argv) != 3:
        usage()
        sys.exit("ERROR: Exactly two arguments expected")
    if not os.path.exists(argv[1]):
        sys.exit("ERROR: Could not find the config file " + argv[1])
    if not os.path.isdir(argv[2]):
        sys.exit("ERROR: Could not find the forecast dir " + argv[2])

def read_config(fname, umcontainers, vnffiles):
    conf = ConfigParser.ConfigParser()
    conf.optionxform = str # Config is case-sensitive
    
    conf.read(fname)

    for section in conf.sections():
        # Proceeding with VNF file defined in section
        options = conf.options(section)
        
        vnf_fname    = ""
        vnf_coords   = ""
        vnf_scodes   = []
        vnf_downsize = "NONE"

        for name in options:
            if conf.get(section, name) == -1:
                print >> sys.stderr, "WARNING: Skipped cfg option: %s" % name
            elif name == 'OutputFile':
                vnf_fname  = conf.get(section, name)
            elif name == 'CoordsFile':
                vnf_coords = conf.get(section, name)
            elif name == 'DownsizeType':
                vnf_downsize = conf.get(section, name)                
            else:
                component_scodes_str = conf.get(section, name)
                component_scodes     = check_scodes(name, component_scodes_str)
                vnf_scodes          += component_scodes
                parse_scodes(name, component_scodes, umcontainers)

        if vnf_fname == "":
            sys.exit("ERROR: No output filename provided in config section \"" + section + "\". Add \"OutputFile\" option to Your config file")

        vnffiles.append(vnffile(section, vnf_fname, vnf_scodes, vnf_coords, vnf_downsize))

def check_scodes(name, scodes_str):
    scodes = scodes_str.split()
    if len(scodes) > 3:
        sys.exit("ERROR: Too many stashcodes (max. 3) provided for component \"" + name + "\"")
    if len(scodes) == 0:
        sys.exit("ERROR: No stashcodes provided for component \"" + name + "\"")

    scode_regex = re.compile('\d\d\d\d\d$')

    for idx, scode in enumerate(scodes):
        if not scode_regex.match(scode):
            sys.exit("ERROR: Provided value \"" + scode + "\" for component \"" + name + "\" is not a valid stashcode")

    return scodes

def parse_scodes(name, scodes, umcontainers):
    veclen  = len(scodes)
    veccomp = 0

    if veclen == 1:
        veccomp = None
        
    for idx, scode in enumerate(scodes):
        if veccomp is not None:
            veccomp = idx
        umcontainers[scode] = umcontainer(scode, name, veclen, veccomp)

def read_files(forecast_path, umcontainers):
    # Filter filenames describing UM fields (scheme given by leszek@icm.edu.pl)
    r = re.compile('\d\d\d\d\d')
    fnames = [ f for f in listdir(forecast_path) if isfile(join(forecast_path, f)) and r.match(f) ]

    for fname in fnames:
        scode = fname[:5]
        # Process files corresponding to stashcodes specified in config file
        if scode in umcontainers.keys():
            umcontainers[scode].umfields.append(umfield(fname, forecast_path))
